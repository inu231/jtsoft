@extends('layouts.home')

@section('og')

<meta property="og:site_name" content="JT Soft">
<meta property="og:url" content="http://jtsoft.com.br">
<meta property="og:type" content="website">
<meta property="og:type" content="article">
<meta property="og:title" content="{{$post->title}}">
<meta property="og:description" content="{{$post->description}}">
<meta property="og:image" content="{{asset('assets/files/'.$post->image)}}">
<meta property="fb:app_id" content="533974290144532">

@endsection

@section('title', $title)

@section('content')

<script src="{{asset('assets/js/site/post.js')}}"></script>

<section id="blog" class="container">
    <div class="center">
        <h2>Notícias</h2>
        <p class="lead">Acompanhe nossas novidades</p>
    </div>

    <div class="blog">
        <div class="row">
            <div class="col-md-8">
                <div class="blog-item">
                    <img class="img-responsive img-blog" src="{{asset('assets/files/'.$post->image)}}" width="100%" alt="" />
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 text-center">
                                <div class="entry-meta">
                                  <span id="publish_date">
                                    {{date('d', strtotime($post->created_at))}}
                                    {{$months[date('m', strtotime($post->created_at))]}}
                                  </span>
                                    <span><i class="fa fa-user"></i> <a href="#"> {{$post->user->name}} </a></span>
                                    <span><i class="fa fa-comment"></i> <a href="{{request()->fullUrl()}}"> <span class="post-comment">0</span> Comments</a></span>
                                    <span><i class="fa fa-heart"></i><a href="javascript:void(0)" id="count-likes-fb">0</a> Likes</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-10 blog-content">
                                <h2>{{$post->title}}</h2>
                                {!! $post->body !!}

                                <div class="post-tags">
                                    <strong>Tag:</strong>
                                    @foreach($tagsList as $tag)
                                      <a href="#">{{$tag}}</a> /
                                    @endforeach
                                </div>


                                <div class="social-media">
                                  <div
                                    class="fb-like"
                                    data-share="true"
                                    data-width="450"
                                    data-show-faces="true">
                                    </div>
                                  </div>

                            </div>
                        </div>
                    </div><!--/.blog-item-->

                    <div class="fb-comments" data-href="{{request()->fullUrl()}}" data-numposts="5" data-width="750"></div>

                    <div class="hidden">
                      <div class="fb-comments-count" data-href="{{request()->fullUrl()}}">0</div> awesome comments</a>
                    </div>

                </div><!--/.col-md-8-->

                @include('components.sidebar')

        </div><!--/.row-->

     </div><!--/.blog-->

</section><!--/#blog-->

@endsection
