<aside class="col-md-4">
    <div class="widget search">
        <form role="form">
            <div class="col-md-10 search-input">
                 <input type="text" class="form-control search_box" autocomplete="off" placeholder="Pesquisar">
            </div>
            <div class="col-md-2 search-btn">
                  <button class="btn btn-search"> <i class="fa fa-search"></i>  </button>
            </div>

        </form>
    </div><!--/.search-->




    <br>
    <!--<div class="widget categories">
        <h3>Recent Comments</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="single_comments">
                    <img src="images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>
                <div class="single_comments">
                    <img src="images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>
                <div class="single_comments">
                    <img src="images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>

            </div>
        </div>
    </div><!--/.recent comments-->


    <div class="widget categories">
        <h3>Categorias</h3>
        <div class="row">
            <div class="col-sm-6">
                <ul class="blog_category">
                    @foreach($categories as $category)
                      <li><a href="{{url('/blog?category='.$category->id)}}"> {{$category->name}}
                        <span class="badge">
                          {{count(Modules\Admin\Entities\Post::where('post_category_id', $category->id)->get())}}
                        </span>
                        </a>
                      </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div><!--/.categories-->

<div class="widget archieve">
        <h3>Arquivo</h3>
        <div class="row">
            <div class="col-sm-12">
                <ul class="blog_archieve">
                    <?php $year = date('Y'); $i = 0; ?>
                    @foreach(array_reverse($last_months) as $key => $month )

                        <li>
                          <a href="{{ is_array($month)
                           ? '/blog?year='.(date('Y'))."&month=".$month['month']
                           : '/blog?year='.(date('Y') -1)."&month=".$month }}">

                            <i class="fa fa-angle-double-right"></i>
                              {{ is_array($month) ? $month['month'] . " " . $month['year'] : $month . " " . (date('Y') - 1)  }}

                              <span class="pull-right">
                                <?= count(Modules\Admin\Entities\Post::whereRaw("year(created_at) = '$year'")->whereRaw("month(created_at) = '$key'")->get()); ?>
                               </span>
                          </a>
                        </li>

                    @endforeach
                    <!--<li><a href="#"><i class="fa fa-angle-double-right"></i> November 2013 <span class="pull-right">(32)</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> October 2013 <span class="pull-right">(19)</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> September 2013 <span class="pull-right">(08)</a></li>-->
                </ul>
            </div>
        </div>
    </div><!--/.archieve-->

    <div class="widget tags">
        <h3>Tag Cloud</h3>
        <ul class="tag-cloud">
            @foreach($tags as $tag)
              <li><a class="btn btn-xs btn-primary" href="{{url('/blog?tag='.$tag->id)}}">{{$tag->name}}</a></li>
            @endforeach
        </ul>
    </div><!--/.tags-->

    <!--<div class="widget blog_gallery">
        <h3>Our Gallery</h3>
        <ul class="sidebar-gallery">
            <li><a href="#"><img src="images/blog/gallery1.png" alt="" /></a></li>
            <li><a href="#"><img src="images/blog/gallery2.png" alt="" /></a></li>
            <li><a href="#"><img src="images/blog/gallery3.png" alt="" /></a></li>
            <li><a href="#"><img src="images/blog/gallery4.png" alt="" /></a></li>
            <li><a href="#"><img src="images/blog/gallery5.png" alt="" /></a></li>
            <li><a href="#"><img src="images/blog/gallery6.png" alt="" /></a></li>
        </ul>
    </div><!--/.blog_gallery-->
</aside>
