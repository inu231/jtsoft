@extends('layouts.home')

@section('title', $title)

@section('content')

<script type='text/javascript'>

var posts = <?php echo json_encode($posts)?>;

</script>

<script src="{{asset('assets/js/site/post.js')}}"></script>

<section id="blog" class="container">
    <div class="center">
        <h2>Notícias</h2>
        <p class="lead">Acompanhe nossas notícias </p>
    </div>

    <div class="blog">
        <div class="row">
             <div class="col-md-8">

               @foreach($posts as $post)



                <div class="blog-item">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 text-center">
                            <div class="entry-meta">
                                <span id="publish_date">
                                  {{date('d', strtotime($post->created_at))}} de
                                  {{$months[date('m', strtotime($post->created_at))]}}
                                </span>
                                <span><i class="fa fa-user"></i> <a href="#"> Felipe Thome </a></span>
                                <!--<span><i class="fa fa-comment"></i>
                                  <span id = "count-likes-fb-{{$post->id}}"></span>
                                </span>
                                <span><i class="fa fa-heart"></i><a href="#">56 Likes</a></span>-->
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-10 blog-content">
                            <a href="#">
                              @if(!empty($post->image))
                                <img class="img-responsive img-blog" src="{{asset('assets/files/'.$post->image)}}" width="100%" alt="" />
                              @endif
                            </a>
                            <h2><a href="blog-item.html">{{$post->title}}</a></h2>
                            <h3>{!! $post->body !!}</h3>
                            <a class="btn btn-primary readmore" href="{{url('/blog/'.$post->slug)}}">Leia mais <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div><!--/.blog-item-->

                @endforeach

              {!! $posts->appends(Input::except('page'))->links() !!}
            </div><!--/.col-md-8-->

          @include('components.sidebar')

        </div><!--/.row-->
    </div>
</section><!--/#blog-->

@endsection
