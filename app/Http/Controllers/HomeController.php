<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Modules\Admin\Entities\Post;
use Modules\Admin\Entities\PostCategory;
use Modules\Admin\Entities\Tag;
use Modules\Admin\Entities\Message;
use Modules\Admin\Business\Calendar;

class HomeController extends Controller
{

    private $calendar;
    private $post;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Calendar $calendar, Post $post)
    {
        //$this->middleware('auth');
        $this->calendar = $calendar;
        $this->post = $post;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'JT Soft | Home';
        return view('pages.new-home', ['title' => $title]);
    }

    /**
     * Mostra a página Sobre
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        $title = 'JT Soft | Sobre';
        return view('pages.sobre', ['title' => $title]);
    }

    /**
     * Mostra a página de serviços
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function services()
    {
        $title = 'JT Soft | Serviços';
        return view('pages.services', ['title' => $title]);
    }

    /**
     * Mostra o portfolio
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function portfolio()
    {
        $title = 'JT Soft | Portfolio';
        return view('pages.portfolio', ['title' => $title]);
    }

    /**
     *  Mostra a página de contatos
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View     *
     */
    public function contact()
    {
        $title = 'JT Soft | Contato';
        return view('pages.contact', ['title' => $title]);
    }

    /**
     *  Mostra a página de blog
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View     *
     */
    public function blog(Request $request)
    {
        $title = 'JT Soft | Notícias';

        //Se a pesquisa foi acionada
        if(count($request->query) > 0)
        {
              $year = $request->get('year');
              $month = $request->get('month');
              $month = $this->calendar->getMonthNumber($month);
              $category = $request->get('category');
              $query = $request->get('query');

              $tag = $request->get('tag');
              $tag = Tag::find($tag);

              // A busca por tags é uma condição totalmente diferente, pois é um inner join, logo ela ficará separada.
              if($tag)
              {
                  $posts = $tag->posts()->orderBy('created_at', 'desc')->paginate(15);
              }
              else
              {
                  $posts = Post::
                            whereRaw(!empty($query) ? "title like '%$query%'" : '1 = 1')
                            ->whereRaw(!empty($query) ? "body like '%$query%'" : '1 = 1')
                            ->whereRaw(!empty($category) ? "post_category_id = '$category'" : '1 = 1')
                            ->whereRaw(!empty($year) ? "year(created_at) = '$year'" : '1 = 1')
                            ->whereRaw( !empty($month) ?"month(created_at) = '$month'" : '1 = 1')
                            ->orderBy('created_at', 'desc')
                            ->paginate(15);
              }
        } else {
              $posts = Post::orderBy('created_at', 'desc')->paginate(15);
        }

        return view('pages.blog', [
            'title' => $title,
            'posts' => $posts,
            'months' => $this->calendar->getMonths(),
            'last_months' => $this->calendar->lastMonths(),
            'categories' => PostCategory::orderBy('name', 'asc')->get(),
            'tags' => Tag::orderBy('name', 'asc')->get(),
        ]);
    }

    /**
     *  Mostra a página de blog
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
     */
    public function showPost($slug)
    {

        $post = Post::where('slug', $slug)->first();
        $tagsList = $post->tags;
        $title = 'JT Soft | ' .$post->title;

        return view('pages.showPost', [
            'title' => $title,
            'post' => $post,
            'months' => $this->calendar->getMonths(),
            'last_months' => $this->calendar->lastMonths(),
            'tagsList' => $tagsList,
            'categories' => PostCategory::orderBy('name', 'asc')->get(),
            'tags' => Tag::orderBy('name', 'asc')->get(),
        ]);
    }

    public function sendMail(Request $request)
    {

        $email = $request->get('email');
        $name = $request->get('name');
        $subject = $request->get('subject');
        $message = $request->get('message');

        $msg = new Message();

        $msg->create([
            'author' => $name,
            'email' => $email,
            'title' => $subject,
            'body' => $message,
        ]);

        $data = [
            'email' => $email,
            'name' => $name,
            'subject' => $subject,
            'message' => $message,
        ];

        $appEmail = 'contato@jtsoft.com.br';

        $mailed = Mail::send('pages.emails.sendmail', ['data' => $data], function ($message) use ($appEmail, $subject, $name, $email) {
            if($message->to($appEmail, $name)->subject($subject));
        });

        return $mailed ? 'Email enviado com sucesso!' : 'Erro. Email não pode ser enviado.';

    }
}
