jQuery(function($) {'use strict',

		url = location.href;

		console.log(location.pathname);

		// Se estiver vizualizando o post.
		if(location.pathname != '/blog') {
			$.ajax({
					type:"get",
					dataType:"HTML",
					data:url,
					url: 'https://graph.facebook.com/fql?q=SELECT%20like_count,%20total_count,%20share_count,%20click_count,%20comment_count%20FROM%20link_stat%20WHERE%20url%20=%20%27' + url + '%27',
					success : function(data) {
							dados = JSON.parse(data);
							$('#count-likes-fb').html(dados.data[0].like_count);
					}
			});

		}

		$('.btn-search').click(function(e){
				e.preventDefault();

				if($('.search_box').val().length > 0) {
						query = $('.search_box').val();
						location.href = "/blog?query="+query;
				}
		});

		// variavel posts foi setada no arquivo blog.blade.php
		if(typeof(posts) != 'undefined' ) {
			for(i = 0; i < posts.data.length; i++) {

					var fullUrl = 'http://' + location.hostname + location.pathname + '/' + posts.data[i].slug;
					var post = {url: fullUrl, id: posts.data[i].id};

					var n = i + 1;

					console.log(n);

					$.ajax({
							type:"get",
							dataType:"HTML",
							data:[n],
							url: 'https://graph.facebook.com/fql?q=SELECT%20like_count,%20total_count,%20share_count,%20click_count,%20comment_count%20FROM%20link_stat%20WHERE%20url%20=%20%27' + fullUrl + '%27',
							success : function(data, success, n, teste) {
									dados = JSON.parse(data);

									console.log(teste);

									$('#count-likes-fb-'+post.id).html(dados.data[0].like_count);
							}
					});
			}
		}



});
