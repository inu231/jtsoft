<?php

namespace Modules\Admin\Business;

use App\Http\Requests;
use Illuminate\Http\Request;
use Storage;


class Upload
{

    /**
     *  Faz upload de uma única imagem
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
     */
    public static function singleFile($file)
    {
          if (gettype($file) == "object")
          {
              $file_name = $file->getClientOriginalName();
              Storage::put($file_name, file_get_contents($file));
              $extension = $file->getClientOriginalExtension();
              $size = Storage::size($file->getClientOriginalName());

              return [
                'extension' => $extension,
                'file_name' => $file_name,
                'file_size' => $size,
              ];
          } else {
            return false;
          }
    }

    /**
     *  Deleta um arquivo do servidor
     *
     * @return bool variable*
     */
    public static function destroyFile($file)
    {
          try
          {
              $isDeleted = unlink('assets/files/'.$file);
          } catch(\Exception $e)
          {
              return 'Arquivo não encontrado';
          }

          return $isDeleted;
    }





}
