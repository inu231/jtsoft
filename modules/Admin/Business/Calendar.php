<?php

namespace Modules\Admin\Business;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;

class Calendar
{
      private $months =  [
          '01' => 'Janeiro',
          '02' => 'Fevereiro',
          '03' => 'Março',
          '04' => 'Abril',
          '05' => 'Maio',
          '06' => 'Junho',
          '07' => 'Julho',
          '08' => 'Agosto',
          '09' => 'Setembro',
          '10' => 'Outubro',
          '11' => 'Novembro',
          '12' => 'Dezembro',
      ];


    /**
     *  Retorna lista de meses
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     *  Retorna os últimos 4 meses
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
     */
    public function lastMonths()
    {
        $last_months = [];

        $current_month = date('m');

        foreach ($this->months as $key => $month) {

            if($current_month == 01)
            {
                $last_months = array_slice($this->months, -3, 3, true);
                $last_months['01'] = ['month' => $this->months['01'], 'year' => date('Y')];
            }
            elseif($current_month == 02)
            {
                $last_months = array_slice($this->months, -2, 2, true);
                $last_months['01'] = ['month' => $this->months['01'], 'year' => date('Y')];
                $last_months['02'] = ['month' => $this->months['02'], 'year' => date('Y')];
            }
            elseif($current_month == 03)
            {
                $last_months = array_slice($this->months, -1, 1, true);
                $last_months['01'] = ['month' => $this->months['01'], 'year' => date('Y')];
                $last_months['02'] = ['month' => $this->months['02'], 'year' => date('Y')];
                $last_months['03'] = ['month' => $this->months['03'], 'year' => date('Y')];
            }
            elseif($key >= $current_month - 3 && $key <= $current_month)
            {
                $last_months[$key] = ['month' => $this->months[$key], 'year' => date('Y')];
            }


        }// endforeach

        return $last_months;
    }

    /**
     *  Retorna o número do mês pelo nome
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
     */
    public function getMonthNumber($month)
    {
        $number = array_search($month, $this->months);
        return $number;
    }


}
