<?php namespace Modules\Admin\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Admin\Entities\Banner;
use Modules\Admin\Entities\Tag;
use Illuminate\Http\Request;
use App\User;
use Validator;
use DateTime;
use Storage;
use Modules\Admin\Business\Upload;

class BannersController extends Controller {

	private $banner;

	public function __construct(Banner $banner)
	{
			$this->banner = $banner;
	}

	/**
	 *  Listagem de banners
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function index(Request $request)
	{

			$isFilter = false;

			if (count($request->query) > 0)
			{
					$isFilter = true;
					$name = $request->get('name');
					$description = $request->get('description');

					$banners = $this->banner
										->whereRaw(!empty($name)? "name LIKE '%$name%'" : '1 = 1')
										->whereRaw(!empty($description)? "description LIKE '%$description%'": '1 = 1')
										->sortable()->paginate(15);
			} else {
					$banners = $this->banner->sortable()->paginate(15);
			}

			return view('admin::banners.index', ['banners' => $banners, 'isFilter' => $isFilter]);
	}

	/**
	 *  Visualizar banner
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function show($id)
	{
			$banner = $this->banner->find($id);
			return view('admin::banners.show', ['banner' => $banner]);
	}

	/**
	 *  Adicionar Banner
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function add()
	{
			return view('admin::banners.add');
	}

	/**
	 *  Salvar banner
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function store(Request $request)
	{

			$validator = Validator::make($request->all(), [
					'name' => 'required',
					'description' => 'required',
					'file' => 'required',
			]);

			if ($validator->fails()) {
					return redirect('/admin/banners/add')
											->withErrors($validator)
											->withInput();
			}


		 $image = Upload::singleFile($request->file('file'));

		 if(gettype($image) != "array")
		 {
			 	return redirect('/admin/banners/add')->with('error-msg', 'A imagem é obrigatória');
		 }

		 $banner = new Banner($request->all());
		 $banner->content_type = $image['extension'];
		 $banner->file_name = $image['file_name'];
		 $banner->file_size = $image['file_size'];


		 if ($banner->save())
		 {
				return redirect('/admin/banners')->with('msg', 'O banner foi salva com sucesso!');
		 } else {
				return redirect('/admin/banners')->with('error-msg', 'O banner não pode ser salva. Por favor, tente novamente.');
		 }
	}

	/**
	 * View de edição banner
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function edit($id)
	{
			$banner = banner::find($id);

			if ($banner)
			{
				return view('admin::banners.edit', ['banner' => $banner]);
			} else {
				return redirect('/admin/banners')->with('error-msg', 'A banner não pode ser encontrada');
			}
	}

	/**
	 *  Alterar banners
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function update(Request $request, $id)
	{
			$banner = $this->banner->find($id);

			$this->validate($request, [
					//'name' => 'required|unique:banners,name,'.$banner->id,
					'name' => 'required',
					'description' => 'required',
			]);


			if($banner)
			{
					$image = Upload::singleFile($request->file('file'));

				  if(gettype($image) == "array")
				  {
							//deleta a imagem associada anteriormente
							Upload::destroyFile($banner->file_name);

							$banner->content_type = $image['extension'];
							$banner->file_name = $image['file_name'];
							$banner->file_size = $image['file_size'];
				  }

					$banner->save();

					if($banner->update($request->all()))
					{
							return redirect('/admin/banners')->with('msg', 'A banner foi atualizada com sucesso!');
					} else {
							return redirect('/admin/banners')->with('error-msg', 'Houve um erro ao atualizar a banner. Por favor, tente novamente.');
					}
			} else {
					return redirect('/admin/banners')->with('error-msg', 'Houve um erro ao efetuar a operação. A banner não foi encontrada');
			}
	}

	/**
	 *  Deletar  banners
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View*
	 */
	public function destroy($id)
	{
			$banner = $this->banner->find($id);

			if($banner)
			{
					//deleta a imagem associada anteriormente
					Upload::destroyFile($banner->file_name);

					if($banner->delete())
					{
							return redirect('/admin/banners')->with('msg', 'banner excluída com sucesso!');
					} else {
							return redirect('/admin/banners')->with('error-msg', 'A banner não pode ser excluída. Por favor, tente novamente!');
					}
			} else {
					return redirect('/admin/banners')->with('error-msg', 'Houve um erro ao efetuar a operação. A banner não foi encontrada.');
			}
	}


}
